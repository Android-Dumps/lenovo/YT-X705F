## msm8937_64-user 10 QKQ1.191224.003 X705F_S001135_210909_ROW release-keys
- Manufacturer: lenovo
- Platform: msm8937
- Codename: YT-X705F
- Brand: Lenovo
- Flavor: msm8937_64-user
- Release Version: 10
- Id: QKQ1.191224.003
- Incremental: X705F_S001135_210909_ROW
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Lenovo/YT-X705F/YT-X705F:10/QKQ1.191224.003/X705F_S001135_210909_ROW:user/release-keys
- OTA version: 
- Branch: msm8937_64-user-10-QKQ1.191224.003-X705F_S001135_210909_ROW-release-keys
- Repo: lenovo/YT-X705F
