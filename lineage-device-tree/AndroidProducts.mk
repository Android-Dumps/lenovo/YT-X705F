#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_YT-X705F.mk

COMMON_LUNCH_CHOICES := \
    lineage_YT-X705F-user \
    lineage_YT-X705F-userdebug \
    lineage_YT-X705F-eng
