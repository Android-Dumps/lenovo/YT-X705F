#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from YT-X705F device
$(call inherit-product, device/lenovo/YT-X705F/device.mk)

PRODUCT_DEVICE := YT-X705F
PRODUCT_NAME := lineage_YT-X705F
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := Lenovo YT-X705F
PRODUCT_MANUFACTURER := lenovo

PRODUCT_GMS_CLIENTID_BASE := android-lenovo-rev2

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="YT-X705F-user 10 QKQ1.191224.003 X705F_S001135_210909_ROW release-keys"

BUILD_FINGERPRINT := Lenovo/YT-X705F/YT-X705F:10/QKQ1.191224.003/X705F_S001135_210909_ROW:user/release-keys
