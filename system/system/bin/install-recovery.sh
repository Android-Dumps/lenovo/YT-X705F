#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:5b8684ce4221a380e90f6695a3be42cb85d96278; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:2dfc3b9882a1d59532cdb5a9623530b13ac07bc7 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:5b8684ce4221a380e90f6695a3be42cb85d96278 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
